package com.handresc1127;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

@RunWith(SerenityRunner.class)
public abstract class base {
    public static String restApiBaseUrl;
    public static EnvironmentVariables environmentVariables;
    public static Actor henry;

    @BeforeClass
    public static void beforeEach() {
        environmentVariables=new SystemEnvironmentVariables();
        restApiBaseUrl= environmentVariables.optionalProperty("baseurl").orElse("https://reqres.in/api");
        henry = Actor.named("Henry Andres Correa Correa the Senior QA Automation")
                .whoCan(CallAnApi.at(restApiBaseUrl));
    }
}
