package com.handresc1127;

import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.rest.interactions.Get;
import org.junit.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.hasItems;

public class WhenGetUsers extends base {

    @Test
    public void listAllUsers() {
        henry.attemptsTo(
                Get.resource("/users")
        );

        henry.should(
                new Consequence[]{
                        seeThatResponse("Response status expected should be 200", response -> response.statusCode(200)),
                        seeThatResponse("Response body schema expected success", response -> response.body(matchesJsonSchemaInClasspath("schema.json"))),
                        seeThatResponse("Response bodu contains gorge, janet, ...", response -> response.body("data.first_name",
                                hasItems("George", "Janet", "Emma", "Eve", "Charles", "Tracey"))),
                }
        );
    }


}
