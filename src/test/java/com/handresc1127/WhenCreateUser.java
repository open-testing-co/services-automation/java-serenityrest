package com.handresc1127;

import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.junit.Test;

import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.notNullValue;

public class WhenCreateUser extends base {
    @Test
    public void createUser() {

        henry.attemptsTo(
                Post.to("/users")
                        .with(request -> request.header("Content-Type", "application/json")
                                .body("{     \"email\": \"h.andresc1127@gmail.com\",\n" +
                                        "    \"first_name\": \"Henry Andres\",\n" +
                                        "    \"last_name\": \"Correa Correa\",\n" +
                                        "    \"avatar\": \"https://reqres.in/img/faces/1-image.jpg\"\n" +
                                        "  }")
                        )
        );
        henry.should(
                new Consequence[]{
                        seeThatResponse("The user should have been successfully created - 201 statusCode", response -> response.statusCode(201)),
                        seeThatResponse("Response body should contains an Id", response -> response.body("id", notNullValue())),
                        seeThatResponse("Response body should contains a created date", response -> response.body("createdAt", notNullValue())),
                }
        );

    }
}

