package com.handresc1127;

import net.serenitybdd.screenplay.Consequence;
import net.serenitybdd.screenplay.rest.interactions.Put;
import org.junit.Test;

import static net.serenitybdd.screenplay.rest.interactions.Delete.from;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.CoreMatchers.notNullValue;

public class WhenModifyUser extends base {

    @Test
    public void updateUser() {
        henry.attemptsTo(
                Put.to("/users")
                        .with(request -> request.header("Content-Type", "application/json")
                                .body("{  \"firstName\" : \"Henry\"," +
                                         "\"lastName\"  : \"Correa\"" +
                                        "}")
                        )
        );

        henry.should(
                seeThatResponse(response -> response.statusCode(200)
                        .body("updatedAt", notNullValue()))
        );

        henry.should(
                new Consequence[]{
                        seeThatResponse("Response status expected should be 200", response -> response.statusCode(200)),
                        seeThatResponse("Response body should contains a updated date", response -> response.body("updatedAt", notNullValue())),
                }
        );

    }

    @Test
    public void deleteUser() {
        henry.attemptsTo(
                from("/users/1")
        );

        henry.should(
                seeThatResponse(response -> response.statusCode(204))
        );
    }
}
